package org.sonar.samples.java.checks;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import org.sonar.check.Rule;
import org.sonar.plugins.java.api.IssuableSubscriptionVisitor;
import org.sonar.plugins.java.api.semantic.Type;
import org.sonar.plugins.java.api.semantic.Symbol.MethodSymbol;
import org.sonar.plugins.java.api.tree.ClassTree;
import org.sonar.plugins.java.api.tree.ExpressionStatementTree;
import org.sonar.plugins.java.api.tree.ExpressionTree;
import org.sonar.plugins.java.api.tree.MemberSelectExpressionTree;
import org.sonar.plugins.java.api.tree.MethodInvocationTree;
import org.sonar.plugins.java.api.tree.MethodTree;
import org.sonar.plugins.java.api.tree.StatementTree;
import org.sonar.plugins.java.api.tree.Tree;
import org.sonar.plugins.java.api.tree.VariableTree;
import org.sonar.plugins.java.api.tree.Tree.Kind;

@Rule(
  key = "PasswordEncryptionRule"
)
public class PasswordEncryptionRule extends IssuableSubscriptionVisitor {
  private static final String PASSWORD_ENCODER = "org.springframework.security.crypto.password.PasswordEncoder";
  private boolean passwordEncoderFieldFound;
  private static final Pattern METHOD_NAME = Pattern.compile(".*Password.*");

  public List<Kind> nodesToVisit() {
    return Collections.singletonList(Kind.METHOD);
  }

  public void visitNode(Tree tree) {
    if (tree.is(new Kind[]{Kind.METHOD})) {
      MethodTree method = (MethodTree)tree;
      String methodName = method.symbol().name().toLowerCase();
      if (METHOD_NAME.matcher(method.simpleName().name()).matches()) {
        this.passwordEncoderFieldFound = false;
        this.checkClassForPasswordEncoderField(method);
        if (!this.passwordEncoderFieldFound) {
          this.checkForPasswordEncoderUsage(method);
        }
      }
    }

  }

  private void checkClassForPasswordEncoderField(MethodTree method) {
    ClassTree classTree = (ClassTree)method.symbol().owner().declaration();
    if (classTree != null) {
      Iterator var3 = classTree.members().iterator();

      while(var3.hasNext()) {
        Tree member = (Tree)var3.next();
        if (member.is(new Kind[]{Kind.VARIABLE})) {
          VariableTree variableTree = (VariableTree)member;
          Type type = variableTree.type().symbolType();
          if (type.is("org.springframework.security.crypto.password.PasswordEncoder")) {
            this.passwordEncoderFieldFound = true;
            break;
          }
        }
      }
    }

  }

  private void checkForPasswordEncoderUsage(MethodTree method) {
    boolean passwordEncoderFound = false;
    if (method.block() != null) {
      Iterator var3 = method.block().body().iterator();

      while(var3.hasNext()) {
        StatementTree statement = (StatementTree)var3.next();
        if (statement.is(new Kind[]{Kind.EXPRESSION_STATEMENT})) {
          ExpressionTree expression = ((ExpressionStatementTree)statement).expression();
          if (expression.is(new Kind[]{Kind.METHOD_INVOCATION})) {
            MethodInvocationTree methodInvocation = (MethodInvocationTree)expression;
            MethodSymbol methodSymbol = (MethodSymbol)methodInvocation.symbol();
            if (methodSymbol.owner().type().fullyQualifiedName().equals("org.springframework.security.crypto.password.PasswordEncoder")) {
              passwordEncoderFound = true;
              break;
            }
          } else if (expression.is(new Kind[]{Kind.MEMBER_SELECT})) {
            MemberSelectExpressionTree memberSelectExpression = (MemberSelectExpressionTree)expression;
            if (memberSelectExpression.expression().symbolType().is("org.springframework.security.crypto.password.PasswordEncoder")) {
              passwordEncoderFound = true;
              break;
            }
          }
        }
      }
    }

    if (!passwordEncoderFound) {
      this.reportIssue(method.simpleName(), "No Password Encryption Detected In Sensitive Methods");
    }

  }
}
