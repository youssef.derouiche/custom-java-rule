package org.sonar.samples.java.checks;

import org.sonar.check.Rule;
import org.sonar.check.RuleProperty;
import org.sonar.plugins.java.api.IssuableSubscriptionVisitor;
import org.sonar.plugins.java.api.tree.*;
import org.sonar.plugins.java.api.tree.Tree.Kind;
import java.util.Collections;
import java.util.List;

@Rule(key = "AuthenticationFailureCheckPfe")
public class AuthenticationFailureCheckPfe extends IssuableSubscriptionVisitor {

  @RuleProperty(
    key = "maxAttempts",
    description = "Maximum number of failed login attempts before lockout",
    defaultValue = "6"
  )
  private int maxAttempts = 6;

  @RuleProperty(
    key = "lockoutDurationMinutes",
    description = "Duration of lockout in minutes",
    defaultValue = "30"
  )
  private int lockoutDurationMinutes = 30;

  private boolean attemptDetectionFound = false;
  private boolean lockoutControlFound = false;

  @Override
  public List<Kind> nodesToVisit() {
    return Collections.singletonList(Kind.CLASS);
  }

  @Override
  public void visitNode(Tree tree) {
    if (tree.is(Kind.CLASS)) {
      ClassTree classTree = (ClassTree) tree;
      if (classTree.simpleName().name().equals("DomainUserDetailsService") ||
        classTree.symbol().name().toLowerCase().contains("Authentication") ||
        classTree.symbol().name().toLowerCase().contains("UserAuthentication") ||
        classTree.symbol().name().toLowerCase().contains("AuthenticateUser")) {
        for (Tree member : classTree.members()) {
          if (member.is(Kind.METHOD)) {
            MethodTree method = (MethodTree) member;
            checkLockoutMechanism(method);
          }
        }
        // Report issues if mechanisms are not found
        if (!attemptDetectionFound) {
          reportIssue(classTree.simpleName(), "Detection of " + maxAttempts + " unsuccessful authentication attempts is missing.");
        }
        if (!lockoutControlFound) {
          reportIssue(classTree.simpleName(), "Proper lockout mechanism after surpassing " + maxAttempts + " failed attempts is missing.");
        }
      }
    }
  }

  private void checkLockoutMechanism(MethodTree method) {
    for (StatementTree statement : method.block().body()) {
      if (statement.is(Kind.IF_STATEMENT)) {
        IfStatementTree ifStatement = (IfStatementTree) statement;
        // Generalize the detection of failed attempts
        if (ifStatement.condition().toString().matches(".*failedAttempts.*>=.*" + maxAttempts + ".*")) {
          attemptDetectionFound = true;
          // Check the then block for lockout actions
          if (ifStatement.thenStatement().is(Kind.BLOCK)) {
            BlockTree ifBlock = (BlockTree) ifStatement.thenStatement();
            for (StatementTree innerStatement : ifBlock.body()) {
              // Generalize lockout action detection
              if (innerStatement.toString().matches(".*updateLockStatus.*true.*,.*")) {
                lockoutControlFound = true;
                break;
              }
            }
          }
        }
      }
    }
  }
}
