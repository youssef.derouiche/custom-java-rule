package org.sonar.samples.java.checks;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.sonar.check.Rule;
import org.sonar.plugins.java.api.IssuableSubscriptionVisitor;
import org.sonar.plugins.java.api.tree.IfStatementTree;
import org.sonar.plugins.java.api.tree.MethodTree;
import org.sonar.plugins.java.api.tree.StatementTree;
import org.sonar.plugins.java.api.tree.Tree;
import org.sonar.plugins.java.api.tree.Tree.Kind;

@Rule(
  key = "PwdRuleCheckPfe"
)
public class PwdRuleCheckPfe extends IssuableSubscriptionVisitor {
  public List<Kind> nodesToVisit() {
    return Collections.singletonList(Kind.METHOD);
  }

  public void visitNode(Tree tree) {
    if (tree.is(new Kind[]{Kind.METHOD})) {
      MethodTree method = (MethodTree)tree;
      if ("registerSimpleUser".equals(method.symbol().name())) {
        this.checkComprehensivePasswordPolicy(method);
      }
    }

  }

  private void checkComprehensivePasswordPolicy(MethodTree method) {
    boolean lengthCheckFound = false;
    boolean characterSetCheckFound = false;
    boolean changeRequirementCheckFound = false;
    boolean passwordReuseCheckFound = false;
    boolean maximumLifetimeCheckFound = false;
    Iterator var7 = method.block().body().iterator();

    while(var7.hasNext()) {
      StatementTree statement = (StatementTree)var7.next();
      if (statement.is(new Kind[]{Kind.IF_STATEMENT})) {
        IfStatementTree ifStatement = (IfStatementTree)statement;
        String condition = ifStatement.condition().toString();
        if (condition.contains("verificationCode.length()") && condition.contains(">=") && condition.contains("16")) {
          lengthCheckFound = true;
        }

        String pattern = "^[a-zA-Z0-9~!@#$%^&*\\(\\)\\-_\\+=\\\\|\\[\\]\\{\\}\\;\\:\\'\\\"\\,\\<\\.\\>/\\?]+$";
        if (condition.contains("verificationCode.matches(\"" + pattern + "\")")) {
          characterSetCheckFound = true;
        }

        if (condition.contains("passwordHistory") && condition.contains("contains")) {
          changeRequirementCheckFound = true;
        }

        if (condition.contains("previousPasswords") && condition.contains("contains")) {
          passwordReuseCheckFound = true;
        }
      }
    }

    if (!lengthCheckFound) {
      this.reportIssue(method.simpleName(), "Password length check is missing. Minimum 16 characters required!");
    }

    if (!characterSetCheckFound) {
      this.reportIssue(method.simpleName(), "Password must include a combination of uppercase, lowercase, numbers, and special characters!");
    }

    if (!changeRequirementCheckFound) {
      this.reportIssue(method.simpleName(), "Check for minimum number of character changes from previous passwords is missing!");
    }

    if (!passwordReuseCheckFound) {
      this.reportIssue(method.simpleName(), "Check for password reuse within the last set number of passwords is missing!");
    }

  }
}

