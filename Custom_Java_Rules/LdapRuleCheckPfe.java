package org.sonar.samples.java.checks;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.sonar.check.Rule;
import org.sonar.plugins.java.api.IssuableSubscriptionVisitor;
import org.sonar.plugins.java.api.tree.AssignmentExpressionTree;
import org.sonar.plugins.java.api.tree.ClassTree;
import org.sonar.plugins.java.api.tree.ExpressionStatementTree;
import org.sonar.plugins.java.api.tree.ExpressionTree;
import org.sonar.plugins.java.api.tree.MethodTree;
import org.sonar.plugins.java.api.tree.StatementTree;
import org.sonar.plugins.java.api.tree.Tree;
import org.sonar.plugins.java.api.tree.VariableTree;
import org.sonar.plugins.java.api.tree.Tree.Kind;

@Rule(
  key = "LdapRuletest"
)
public class LdapRuleCheckPfe extends IssuableSubscriptionVisitor {
  private static final List<String> LDAP_VARIABLES = Arrays.asList("ldapCtxFactory", "ldapAuthentication", "ldapReferral", "ldapUrl", "ldapPrincipal");
  private static final Pattern LDAP_CONTEXT_PATTERN = Pattern.compile("InitialDirContext|DirContext");
  private static final Pattern LDAP_SEARCH_PATTERN = Pattern.compile("\\.search\\(");

  public List<Kind> nodesToVisit() {
    return Arrays.asList(Kind.CLASS, Kind.METHOD);
  }

  public void visitNode(Tree tree) {
    if (tree.is(new Kind[]{Kind.CLASS})) {
      ClassTree classTree = (ClassTree)tree;
      this.checkClassName(classTree);
    }

  }

  private void checkClassName(ClassTree classTree) {
    String className = classTree.simpleName().name();
    if (className.contains("MiddleWareImpl")) {
      Iterator var3 = classTree.members().iterator();

      while(var3.hasNext()) {
        Tree member = (Tree)var3.next();
        if (member.is(new Kind[]{Kind.METHOD})) {
          MethodTree methodTree = (MethodTree)member;
          this.checkLdapMethod(methodTree);
        }
      }
    }

  }

  private void checkLdapMethod(MethodTree methodTree) {
    boolean ldapVariablesUsed = false;
    boolean contextCreated = false;
    boolean searchPerformed = false;
    Iterator var5 = methodTree.block().body().iterator();

    while(var5.hasNext()) {
      StatementTree statement = (StatementTree)var5.next();
      if (statement.is(new Kind[]{Kind.EXPRESSION_STATEMENT})) {
        ExpressionTree expression = ((ExpressionStatementTree)statement).expression();
        if (expression.is(new Kind[]{Kind.ASSIGNMENT})) {
          AssignmentExpressionTree assignment = (AssignmentExpressionTree)expression;
          String variableName = assignment.variable().toString();
          if (LDAP_VARIABLES.contains(variableName)) {
            ldapVariablesUsed = true;
          }
        }
      }
    }

    var5 = ((List)methodTree.block().body().stream().filter((statementx) -> {
      return statementx.is(new Kind[]{Kind.VARIABLE});
    }).map((statementx) -> {
      return (VariableTree)statementx;
    }).collect(Collectors.toList())).iterator();

    while(var5.hasNext()) {
      VariableTree variableTree = (VariableTree)var5.next();
      if (LDAP_CONTEXT_PATTERN.matcher(variableTree.initializer() != null ? variableTree.initializer().toString() : "").find()) {
        contextCreated = true;
      }
    }

    var5 = ((List)methodTree.block().body().stream().filter((statementx) -> {
      return statementx.is(new Kind[]{Kind.EXPRESSION_STATEMENT});
    }).map((statementx) -> {
      return (ExpressionStatementTree)statementx;
    }).collect(Collectors.toList())).iterator();

    while(var5.hasNext()) {
      ExpressionStatementTree statementTree = (ExpressionStatementTree)var5.next();
      if (LDAP_SEARCH_PATTERN.matcher(statementTree.expression().toString()).find()) {
        searchPerformed = true;
      }
    }

    if (!ldapVariablesUsed || !contextCreated || !searchPerformed) {
      this.reportIssue(methodTree, "Method should perform LDAP authentication but no LDAP usage was detected!");
    }

  }
}
