package org.sonar.samples.java.checks;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.sonar.check.Rule;
import org.sonar.plugins.java.api.IssuableSubscriptionVisitor;
import org.sonar.plugins.java.api.tree.MethodTree;
import org.sonar.plugins.java.api.tree.Tree;
import org.sonar.plugins.java.api.tree.VariableTree;
import org.sonar.plugins.java.api.tree.Tree.Kind;

@Rule(
  key = "HTTPSandTLSEnforcement"
)
public class HTTPSandTLSEnforcementRule extends IssuableSubscriptionVisitor {
  private static final Pattern METHOD_NAME_PATTERN = Pattern.compile(".*Rest.*");

  public List<Kind> nodesToVisit() {
    return Collections.singletonList(Kind.METHOD);
  }

  public void visitNode(Tree tree) {
    MethodTree methodTree = (MethodTree)tree;
    if (METHOD_NAME_PATTERN.matcher(methodTree.simpleName().name()).matches()) {
      this.checkSSLConfiguration(methodTree);
    }

  }

  private void checkSSLConfiguration(MethodTree methodTree) {
    boolean sslContextFound = false;
    boolean closeableHttpClientFound = false;
    boolean sslConnectionSocketFactoryFound = false;
    boolean trustStrategyFound = false;
    Iterator var6 = ((List)methodTree.block().body().stream().filter((statement) -> {
      return statement.is(new Kind[]{Kind.VARIABLE});
    }).map((statement) -> {
      return (VariableTree)statement;
    }).collect(Collectors.toList())).iterator();

    while(var6.hasNext()) {
      VariableTree variableTree = (VariableTree)var6.next();
      String typeName = variableTree.type().symbolType().fullyQualifiedName();
      if (typeName.equals("javax.net.ssl.SSLContext")) {
        sslContextFound = true;
      } else if (typeName.equals("org.apache.http.impl.client.CloseableHttpClient")) {
        closeableHttpClientFound = true;
      } else if (typeName.equals("org.apache.http.conn.ssl.SSLConnectionSocketFactory")) {
        sslConnectionSocketFactoryFound = true;
      } else if (typeName.equals("org.apache.http.ssl.TrustStrategy")) {
        trustStrategyFound = true;
      }
    }

    if (!sslContextFound) {
      this.reportIssue(methodTree, "Method does not configure SSLContext. Ensure SSLContext is properly configured to implement HTTPS and TLS.");
    }

    if (!closeableHttpClientFound) {
      this.reportIssue(methodTree, "Method does not configure CloseableHttpClient. Ensure CloseableHttpClient is properly configured to use SSL/TLS settings.");
    }

    if (!sslConnectionSocketFactoryFound) {
      this.reportIssue(methodTree, "Method does not configure SSLConnectionSocketFactory. Ensure SSLConnectionSocketFactory is used to establish a trusted communication channel.");
    }

    if (!trustStrategyFound) {
      this.reportIssue(methodTree, "Method does not configure TrustStrategy. Ensure TrustStrategy is used to validate certificates properly.");
    }

  }
}
